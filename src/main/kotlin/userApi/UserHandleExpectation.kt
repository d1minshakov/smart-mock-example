package userApi

import client.MockClient
import com.google.gson.Gson
import userApi.dto.*
import org.mockserver.matchers.TimeToLive
import org.mockserver.matchers.Times
import org.mockserver.mock.action.ExpectationResponseCallback
import org.mockserver.mock.Expectation
import org.mockserver.model.*
import org.mockserver.model.JsonBody.json
import utils.getIsoCurrentDate
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.absoluteValue

const val TTL_SEC: Long = 600

class UserHandleExpectation : ExpectationResponseCallback {
    override fun handle(httpRequest: HttpRequest): HttpResponse {
        val gson = Gson()
        val mockServerClient = MockClient()

        // Convert POST payload to data structure
        val userPayload: UserPOSTRequestDTO? =
            gson.fromJson(httpRequest.bodyAsJsonOrXmlString, UserPOSTRequestDTO::class.java)

        // Create expectation for GET /users/{userId}
        val userUUID = UUID.randomUUID().toString()
        val userGETResponse = UserGETResponseDTO(
            userUid = userUUID,
            currency = userPayload?.currency,
            region = userPayload?.region,
            serverCode = userPayload?.server_code,
            createdDate = getIsoCurrentDate()
        )
        val userExpectation = Expectation.`when`(
            HttpRequest.request()
                .withMethod("GET")
                .withPath("/api/v1/users/${userUUID}"),
            Times.unlimited(), TimeToLive.exactly(TimeUnit.SECONDS, TTL_SEC)
        ).thenRespond(
            HttpResponse.response()
                .withContentType(MediaType.APPLICATION_JSON)
                .withBody(json(gson.toJson(userGETResponse, UserGETResponseDTO::class.java)))
        )

        // Create expectation for GET /users/{userId}/accounts/{accountId}
        val accountId = userUUID.hashCode().absoluteValue
        val accountGETResponse = AccountGETResponseDTO(
            id = accountId,
            userUid = userUUID,
            currency =  userPayload?.currency,
            status = "ACTIVE",
            expired = false
        )
        val userAccountsExpectation = Expectation.`when`(
            HttpRequest.request()
                .withMethod("GET")
                .withPath("/api/v1/users/${userUUID}/accounts/${accountId}"),
            Times.unlimited(), TimeToLive.exactly(TimeUnit.SECONDS, TTL_SEC)
        ).thenRespond(
            HttpResponse.response()
                .withContentType(MediaType.APPLICATION_JSON)
                .withBody(json(gson.toJson(accountGETResponse, AccountGETResponseDTO::class.java)))
        )

        // Prepare response for current callback
        val userPOSTCallbackResponse = UserPOSTResponseDTO(
            userId = userUUID,
            account = accountId,
            serverCode = userPayload?.server_code,
            region = userPayload?.region
        )

        // Store expectations in Mockserver by one request
        mockServerClient.setExpectations(
            listOf<Expectation>(
                userExpectation,
                userAccountsExpectation
            )
        )
        return HttpResponse.response()
            .withStatusCode(HttpStatusCode.CREATED_201.code())
            .withContentType(MediaType.APPLICATION_JSON)
            .withBody(json(gson.toJson(userPOSTCallbackResponse, UserPOSTResponseDTO::class.java)))
    }
}