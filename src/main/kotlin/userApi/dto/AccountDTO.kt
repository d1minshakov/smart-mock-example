package userApi.dto

data class AccountGETResponseDTO(
    val id: Int? = null,
    val currency: String? = null,
    val userUid: String? = null,
    val status: String? = null,
    val expired: Boolean? = null
)
