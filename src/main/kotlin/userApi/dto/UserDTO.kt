package userApi.dto

data class UserPOSTRequestDTO(
    val user_id: String? = null,
    val currency: String? = null,
    val region: String? = null,
    val server_code: String? = null
)

data class UserPOSTResponseDTO(
    val userId: String? = null,
    val account: Int? = null,
    val serverCode: String? = null,
    val region: String? = null
)

data class UserGETResponseDTO(
    val account: Int? = null,
    val currency: String? = null,
    val userUid: String? = null,
    val region: String? = null,
    val serverCode: String? = null,
    val createdDate: String? = null
)
