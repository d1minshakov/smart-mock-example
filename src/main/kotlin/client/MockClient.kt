package client

import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import org.mockserver.mock.Expectation
import java.net.URL
import java.util.concurrent.TimeUnit


class MockClient {

    private val mockBaseURL = "http://localhost:1080/mockserver/expectation"

    private fun getHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.readTimeout(30, TimeUnit.SECONDS)
        builder.writeTimeout(30, TimeUnit.SECONDS)
        return builder.build()
    }

    fun setExpectations(expectation: List<Expectation>) {
        val url = URL(mockBaseURL)
        val client = getHttpClient()
        val mediaType = "application/json; charset=utf-8".toMediaType()
        val body = expectation.toString().toRequestBody(mediaType)
        val request = Request.Builder().url(url).put(body).build()
        client.newCall(request).execute().close()
    }
}
