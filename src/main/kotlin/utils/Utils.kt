package utils

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

fun getIsoCurrentDate(): String? {
    val tz = TimeZone.getTimeZone("UTC")
    val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")
    df.timeZone = tz
    return df.format(Date())
}